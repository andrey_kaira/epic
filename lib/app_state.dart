import 'package:flutter/material.dart';
import 'package:flutterepicpractice/anime.dart';

class AppState {
  final List<Anime> animeList;

  AppState({
    this.animeList,
  });

  factory AppState.initState() => AppState(
        animeList: [],
      );
  @override
  String toString() {
    return '${animeList.length}';
  }
}
