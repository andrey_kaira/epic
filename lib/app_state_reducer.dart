import 'dart:collection';
import 'package:flutter/material.dart';

class PhotosState {
  final List<ImageDto> images;
  PhotosState (this.images);
  factory PhotosState.initial() => PhotosState ([]);
  PhotosState copyWith(List<ImageDto> images) {
    return PhotosState(
      images ?? this.images,
    );
  }
  PhotosState reducer(dynamic action) {
    print('PHOTO STATE IMAGES${images.length}');
    print(action.runtimeType);
    return Reducer<PhotosState>(
      actions: HashMap.from({
        SaveImagesAction: (dynamic action) => saveImages((action as SaveImagesAction).images),
      }),
    ).updateState(action, this);
  }
  PhotosState saveImages(List<ImageDto> images) {
    return PhotosState(images);
  }
}