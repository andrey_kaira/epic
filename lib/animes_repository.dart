import 'package:flutter/cupertino.dart';
import 'package:flutterepicpractice/service_anime.dart';

import 'anime.dart';

class AnimeRepository{

  Future<List<Anime>> getAnime(dynamic body){
    return ServiceAnime()
        .fillData(body);
  }
}