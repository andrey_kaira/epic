import 'package:flutter/cupertino.dart';

import 'anime.dart';

class ServiceAnime {
  Future<List<Anime>> fillData(Map<String, dynamic> body)
  async {
    List<Anime> animeList = new List<Anime>();
    body.forEach((prodId, prodData) {
      if (prodId == 'data' || prodId == 'included') {
        (prodData as List<dynamic>).forEach((value) {
          (value as Map<dynamic, dynamic>).forEach((keyAnime, anime) {
            if (keyAnime == 'attributes') {
              Anime rp = new Anime();
              (anime as Map<dynamic, dynamic>)
                  .forEach((keyAtributes, animeAtributes) {
                if (keyAtributes == 'canonicalTitle') {
                  rp.name = animeAtributes.toString();
                }
                if (keyAtributes == 'posterImage') {
                  (animeAtributes as Map<dynamic, dynamic>)
                      .forEach((keyPosters, valuePosters) {
                    if (keyPosters == 'medium') {
                      rp.path = valuePosters.toString();
                    }
                  });
                }
              });
              animeList.add(rp);
            }
          });
        });
      }
    });
    return await animeList;
  }
}
