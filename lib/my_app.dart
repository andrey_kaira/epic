import 'package:flutter/material.dart';
import 'package:flutterepicpractice/action.dart';
import 'package:flutterepicpractice/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

import 'app_state_reducer.dart';
import 'epic.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final store = Store<AppState>(
      reducer,
      initialState: AppState.initState(),
      middleware: [EpicMiddleware(jokerEpic)],
    );

    store.onChange.listen(print);

    store.dispatch(SetAnimeList);
    return Container();
  }
}
