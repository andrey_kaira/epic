import 'package:flutter/material.dart';

import 'my_app.dart';

GlobalKey counterState;

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      navigatorKey: counterState,
      home: MyApp(),
    );
  }
}
