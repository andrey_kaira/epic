import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import 'app_state.dart';
class PhotosEpics {
  static final indexEpic = combineEpics<AppState>([
    getImagesEpic,
    setImagesEpic,
  ]);
  static Stream<dynamic> getImagesEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    print('GetImagesEpic');
    return actions.whereType<GetImagesAction>().switchMap((action) {
      return Stream.fromFuture(
        UnsplashImageRepository.instance
            .getImages()
            .then((List<ImageDto> images) {
          if (images == null) {
            print('Images is null');
            return EmptyAction();
          }
          print('images ${images.length}');
          print(images[0].id);
          //   return PhotosState(images);
          return SaveImagesAction(
            images: images,
          );
        }),
      );
    });
  }
//  PhotosState increment() {
//    return PhotosState(this.c);
//  }
  static Stream<dynamic> setImagesEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<SetInitImagesAction>().switchMap((action) {
      return Stream.fromIterable([
        SaveImagesAction(images: action.images),
      ]);
    });
  }
}