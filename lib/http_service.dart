import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

enum HttpType {
  post,
  patch,
  put,
  get,
}

class HttpService {
  static Future<Map<String, dynamic>> request(
      HttpType httpType,
      String link,
      Map<String, dynamic> body,
      Map<String, dynamic> params) async {
    var response;
    switch (httpType) {
      case HttpType.post:
        response = await http.post(
          link,
          body: body,
          headers: params,
        );
        break;
      case HttpType.get:
        response = await http.get(link);
        body = json.decode(response.body) as Map<String, dynamic>;
        return body;
        break;
      case HttpType.patch:
        response = await http.patch(
          link,
          body: body,
          headers: params,
        );
        break;
    }
    // print(body);
    final responseData = json.decode(response.body);
    if (responseData['error'] != null) {
      print(responseData['error']['message']);
    }
  }
}
